﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="HW Drivers" Type="Folder">
			<Item Name="Spectrometer.lvclass" Type="LVClass" URL="../Source/Spectrometer/Spectrometer.lvclass"/>
			<Item Name="Temperature Interface.lvclass" Type="LVClass" URL="../Source/Temperature Interface/Temperature Interface.lvclass"/>
		</Item>
		<Item Name="Steps" Type="Folder">
			<Item Name="Acquire Spectra.lvclass" Type="LVClass" URL="../Source/Steps/Acquire Spectra/Acquire Spectra.lvclass"/>
			<Item Name="Read Thermocouple.lvclass" Type="LVClass" URL="../Source/Steps/Read Thermocouple/Read Thermocouple.lvclass"/>
			<Item Name="SequenceStep.lvclass" Type="LVClass" URL="../Source/SequenceStep/SequenceStep.lvclass"/>
			<Item Name="Set Heater Output.lvclass" Type="LVClass" URL="../Source/Steps/Set Heater Output/Set Heater Output.lvclass"/>
		</Item>
		<Item Name="Support Classes" Type="Folder">
			<Item Name="TextFile Writer.lvclass" Type="LVClass" URL="../Source/TextFile Writer/TextFile Writer.lvclass"/>
			<Item Name="UI DBL Reference.lvclass" Type="LVClass" URL="../Source/UI DBL Reference/UI DBL Reference.lvclass"/>
			<Item Name="UI Graph Reference.lvclass" Type="LVClass" URL="../Source/UI Graph Reference/UI Graph Reference.lvclass"/>
		</Item>
		<Item Name="DataPersistance.lvclass" Type="LVClass" URL="../Source/DataPersistance/DataPersistance.lvclass"/>
		<Item Name="Sequence Datafile Writer.lvclass" Type="LVClass" URL="../Source/Sequence Datafile Writer/Sequence Datafile Writer.lvclass"/>
		<Item Name="Sequence Validation.lvclass" Type="LVClass" URL="../Source/Sequence Validation/Sequence Validation.lvclass"/>
		<Item Name="SequenceUI Persistance.lvclass" Type="LVClass" URL="../Source/SequenceUI Persistance/SequenceUI Persistance.lvclass"/>
		<Item Name="Main.vi" Type="VI" URL="../Source/Examples/Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Random Number (Range) DBL.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) DBL.vi"/>
				<Item Name="Random Number (Range) I64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) I64.vi"/>
				<Item Name="Random Number (Range) U64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) U64.vi"/>
				<Item Name="Random Number (Range).vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range).vi"/>
				<Item Name="sub_Random U32.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/sub_Random U32.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
